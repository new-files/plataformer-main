using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton : MonoBehaviour
{
    public GameObject bloqueAMover;
    public float velocidadMovimiento = 5f;

    private bool jugadorEnContacto = false;

    void Update()
    {
       
        if (jugadorEnContacto && Input.GetKeyDown(KeyCode.F))
        {
            
            bloqueAMover.transform.Translate(Vector3.right * velocidadMovimiento * Time.deltaTime);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            jugadorEnContacto = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            jugadorEnContacto = false;
        }
    }
}
