using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MovPlataforma : MonoBehaviour
{
    public Vector3 initialPosition;
    public float maxYOffset;
    public float speed;

    private Vector3 targetPosition;
    private bool movingToTarget = true;

    void Start()
    {
        initialPosition = transform.position;
        targetPosition = initialPosition + Vector3.up * maxYOffset;
    }

    void Update()
    {
        MovePlatform();
    }

    void MovePlatform()
    {
        Vector3 direction = movingToTarget ? Vector3.up : Vector3.down;
        transform.Translate(direction * speed * Time.deltaTime);

        if ((movingToTarget && transform.position.y >= targetPosition.y) ||
            (!movingToTarget && transform.position.y <= initialPosition.y))
        {
            movingToTarget = !movingToTarget;
        }
    }
  
}

