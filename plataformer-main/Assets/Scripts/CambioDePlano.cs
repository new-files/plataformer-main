using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioDePlano : MonoBehaviour
{
    public float distanciaTraslacion = 1.0f;
    private bool trasladado = false;
    public LayerMask Teleport; 

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            RaycastHit hit;
            
            if (Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity, Teleport))
            {
                if (trasladado)
                {
                    transform.Translate(Vector3.up * -distanciaTraslacion);
                }
                else
                {
                    transform.Translate(Vector3.up * distanciaTraslacion);
                }

                trasladado = !trasladado;
            }
        }
    }
}
