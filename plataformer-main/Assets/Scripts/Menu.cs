using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void Empezar(string Plataformer)
    {
        SceneManager.LoadScene(Plataformer);
    }

  public void Salir()
    {
        Application.Quit();
    }
}
