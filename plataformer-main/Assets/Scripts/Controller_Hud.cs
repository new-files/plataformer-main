﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Controller_Hud : MonoBehaviour
{
    public Text gameOverText;
    public int nextSceneName;

    void Start()
    {
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (GameManager.gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over" ;
            gameOverText.gameObject.SetActive(true);
        }
        if (GameManager.winCondition)
        {
            SceneManager.LoadScene(nextSceneName); 
        }
    }
}
