using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIzquierdo : MonoBehaviour
{
    public GameObject proyectilIzquierdoPrefab; // Prefab del proyectil izquierdo
    public float intervaloDisparo = 2f; // Intervalo entre disparos

    void Start()
    {
        InvokeRepeating("Disparar", 0f, intervaloDisparo);
    }

    void Disparar()
    {
        // Instanciar el proyectil izquierdo
        GameObject proyectilIzquierdo = Instantiate(proyectilIzquierdoPrefab, transform.position, Quaternion.identity);

        // Obtener el componente ProjectileIzquierdo del proyectil
        ProjectileIzquierdo projectileComponent = proyectilIzquierdo.GetComponent<ProjectileIzquierdo>();

        // Asignar la direcci�n al proyectil
        if (projectileComponent != null)
        {
            projectileComponent.SetDireccion(Vector3.left);
        }
    }
}