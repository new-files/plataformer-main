using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileIzquierdo : MonoBehaviour
{
    public float velocidad = 15f;
    public float tiempoDeVida = 6f;
    void Start()
    {
      
        Destroy(gameObject, tiempoDeVida);
    }
    void Update()
    {     
        transform.Translate(Vector3.left * velocidad * Time.deltaTime);
    }
    public void SetDireccion(Vector3 nuevaDireccion)
    {
     
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
          
            GameManager.gameOver = true;
        }
        else
        {
         
            Destroy(gameObject);
        }
    }
}